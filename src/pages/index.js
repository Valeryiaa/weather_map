import React, { useState, useEffect } from 'react';
import './index.sass';
import { Grid, Icon } from 'semantic-ui-react';
import { YMaps, Map, Placemark, SearchControl } from 'react-yandex-maps';
import Forecast from "@components/forecast/forecast";

const SearchControlOpt = {
  geoObjectStandardPreset: 'islands#redCircleDotIcon',
  provider: 'yandex#search'
}

const WeatherMap = () => {

  const [searchControl, setSearchControl] = useState(null);
  const [place, setPlace] = useState(' ');
  const [coords, setCoords] = useState([55.7558, 37.6173]);
  const [center, setCenter] = useState([55.75, 37.57])
  const [forecast, setForecast] = useState({
    temp: ' ',
    icon: ' ',
    city: ' ' ,
    windSpeed: ' ',
    humidity: ' '
  });
  const [inProgress, setProgress] = useState(false)

  useEffect(() => { getWeather([55.7558, 37.6173])}, [])

  const loadSuggest = ymaps => {
    const test = new ymaps.SuggestView("suggest");
    test.events.add("select", e => setPlace(e.get('item').value))
  }
  const handleChange = e => {
    const {value} = e.target;
    setPlace(value)
  };

  const onMapClick =  async e => {
    setCoords(e.get("coords"));
    setCenter(e.get("coords"));
    try {
       const data = await fetch(`https://cors-anywhere.herokuapp.com/https://geocode-maps.yandex.ru/1.x/?apikey=6a754c39-550d-46d4-826b-2679d936b461&format=json&geocode=${e.get("coords")[1]},${e.get("coords")[0]}`)
        .then(res => { return res.json() })
        .then(data => { return data });

        const location = data.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.formatted;
        setPlace(location);
        await getWeather(e.get("coords"))
      } catch (err) {
        console.log(err)
      }
  };

  const search = async () => {
    try {
      await searchControl.search(place)
        .then(() => {
          const geoObjectsArray = searchControl.getResultsArray();
          return geoObjectsArray[0].geometry._coordinates;
        })
      .then(coords => {
        setCoords(coords)
        setCenter(coords)
        getWeather(coords)
      })
    } catch (e) {
      console.log(e)
    }
  }

  const handleSubmit = e => { if (e.keyCode === 13) search() }

  const getWeather = async info => {
    try {
     setProgress(true)
     const data = await fetch(`https://cors-anywhere.herokuapp.com/https://api.weather.yandex.ru/v2/forecast?lat=${info[0]}&lon=${info[1]}&extra=true`,
        {headers: {'X-Yandex-API-Key': 'f4396c2f-33ec-4e66-8e2e-6b48f6342291'}})
       .then(res => { return res.json() })
       .then(data => { return data });
      const forecastInfo = {
        temp: data.fact.temp,
        icon: data.fact.icon,
        city: data.geo_object.locality ? data.geo_object.locality.name : data.geo_object.country.name,
        windSpeed: data.fact.wind_speed,
        humidity: data.fact.humidity
      }
      setForecast(forecastInfo)
      setProgress(false)
    } catch (err) {
      console.log(err)
    }
  }

    return (
      <Grid centered>
        <Grid.Row>
          <input className="search_input" type="text" id="suggest"
                 autoFocus value={place}
                 onKeyDown={handleSubmit}
                 onChange={handleChange}
          />
        <Icon name='search' size='large' color='grey' onClick={search}/>
        </Grid.Row>
        <Grid.Row>
          <Grid columns={3} divided stackable centered>
            <Grid.Row>
              <Grid.Column width={8}>
                <Forecast data={forecast} inProgress={inProgress}/>
              </Grid.Column>
              <Grid.Column width={8}>
                <YMaps query={{
                  lang: 'en_RU',
                  ns: "use-load-option",
                  apikey: "6a754c39-550d-46d4-826b-2679d936b461"
                }}>
                  <div className='map'>
                    <Map modules={["geolocation", "geocode", "SuggestView"]}
                         onLoad={ymaps => loadSuggest(ymaps)}
                         onClick={onMapClick}
                         width='100%' style={{minHeight: 550, minWidth: 500}}
                         defaultState={{ center: center, zoom: 9}}>
                      <SearchControl options={SearchControlOpt} instanceRef={ref => setSearchControl(ref)}/>
                      <Placemark geometry={coords} />
                    </Map>
                  </div>
                </YMaps>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Row>
      </Grid>
    );
};

export default WeatherMap;
