import React from 'react';
import './index.sass';
import 'moment/locale/ru';
import { Grid } from "semantic-ui-react";
import Loader from '@components/loader'
const moment = require('moment');
moment.locale('ru')

const Forecast = props => {

  const { data, inProgress } = props;
  const icon = data.icon !== ' ' ? `https://yastatic.net/weather/i/icons/blueye/color/svg/${data.icon}.svg` : null
  const date = {
    weekDay: moment().format('dddd'),
    date: moment().format('LL'),
    time: moment().format('LT')
  }

  return (

      <Grid textAlign='center' style={{ height: 555}}>
        <Grid.Row columns={2} verticalAlign='middle' stretched>
          <Grid.Column stretched width={8}>
            <div className="top-left">
              <h2 id="day">{date.weekDay[0].toUpperCase() + date.weekDay.substring(1)}</h2>
              <h3 id="date">{date.date}</h3>
              <h3 id="time">{date.time}</h3>
            </div>
          </Grid.Column>
          <Grid.Column stretched width={8}>
            <div className="weather-wrapper">
                <div className="weather-card">
                  {inProgress
                    ? <Loader/>
                    : <>
                        <img src={icon} alt=""/>
                          <h1>{data.temp}</h1>
                          <p>{data.city}</p>
                      </>
                  }
                </div>
                {!inProgress &&
                  <div className="info">
                    <span className="rain">{data.humidity}</span>
                    <span className="wind">{data.windSpeed}</span>
                  </div>
                }
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>

  )
}

export default Forecast;