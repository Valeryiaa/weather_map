import React from 'react';
import './index.sass';

const Loader = () => {
  return (
    <div className="lds-facebook">
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}

export default Loader;