import React from 'react';
import { Switch, Route } from 'react-router-dom';
import WeatherMap from "@pages";

const AppRouter = props => {
  return (
    <Switch>
      <Route path="/"><WeatherMap/></Route>
    </Switch>
  );
};

export default AppRouter;