import React from 'react';
import './App.css';
import AppRouter from '@routes/AppRouter';
import 'semantic-ui-css/semantic.min.css';
import { Container } from 'semantic-ui-react'

const App = props => {

  return (
    <div className="App">
      <Container textAlign='justified'>
        <AppRouter/>
      </Container>
    </div>
  );
};

export default App;
